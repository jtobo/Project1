package com.revature.controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.plaf.synth.SynthSeparatorUI;

import org.apache.catalina.servlets.DefaultServlet;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.services.TicketService;

public class TicketController extends DefaultServlet {

	private static final long serialVersionUID = -6661614483732743154L;
	private static final Logger Log = Logger.getRootLogger();
	private static final TicketService service= new TicketService();

	@Override
	protected void	service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Avoid CORS error
		response.addHeader("Access-Control-Allow-Origin", "*");
		
		//Delegate back to the parent service method for HTTP method delegation
		super.service(request, response);
	}

	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) {

		Log.warn(request.getRequestURI());
		String context = request.getRequestURI().substring(request.getContextPath().length()+ "TicketController/".length());
		
		
		System.out.println("context is " + context);

		if(context.length() <= 1) {
			System.out.println("We in here");
			handleGetAllTickets(response);
			return;
		}

		try {
			// if the Employee is trying to view his past tickets
			int id= Integer.parseInt(context.split("/")[1]);
			handleGetEmpTickets(id,response);
			return;
		} catch(NumberFormatException e) {
			e.printStackTrace();
		}
		
		response.setStatus(404);


	}


	private void handleGetEmpTickets(int id, HttpServletResponse response) {
		// TODO Auto-generated method stub
		ObjectMapper om= new ObjectMapper();
//		String s="1,233.43";
//		float d= Float.parseFloat(s);
//		System.out.println(d);
		try {
			om.writeValue(response.getWriter(), service.viewHistory(id));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	private void handleGetAllTickets(HttpServletResponse response) {
		// TODO Auto-generated method stub
		ObjectMapper om= new ObjectMapper();
		try {
			om.writeValue(response.getWriter(), service.viewTickets());
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
}
