package com.revature.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
//import java.util.List;

import org.apache.catalina.filters.AddDefaultCharsetFilter;

//import com.revature.beans.Employee;
import com.revature.util.ConnectionUtil;
import com.revature.util.PasswordUtil;

public class EmployeeDaoImpl implements EmployeeDao {
	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();
	private PasswordUtil passwordUtil = new PasswordUtil();

	@Override
	public void addTicket(double amount, String tickDesc, byte[] tickReceipt, int tickHolder, int tickTypeId,
			int empId) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO ticket(amount,tick_desc,tick_receipt,tick_holder,tick_status_id,tick_type_id)"
					+ " VALUES (?,?,?,?,3,?);";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setDouble(1, amount);
			ps.setString(2, tickDesc);
			ps.setBytes(3, tickReceipt);
			ps.setInt(4, tickHolder);
			ps.setInt(5, tickTypeId);
			ps.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void approveTicket(int tickId, int employeeId) {
		// TODO Auto-generated method stub
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO ticket(tick_solver,tick_status_id) VALUES (?,1) WHERE tick_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, employeeId);
			ps.setInt(2, tickId);
			ps.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void denyTicket(int tickId, int employeeId) {
		// TODO Auto-generated method stub
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO ticket(tick_solver,tick_status_id) VALUES (?,2) WHERE tick_id=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, employeeId);
			ps.setInt(2, tickId);
			ps.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public boolean checkEmployee(String username, String password) {
		try (Connection conn = connectionUtil.getConnection()) {

			String sql = "SELECT (username,password) FROM employee " + " WHERE username=? and password =?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			try {
				ps.setString(2, passwordUtil.generateStrongPasswordHash(password, getSalt(username)));
			} catch (NoSuchAlgorithmException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ResultSet results = ps.executeQuery();
			if (results.next()) {
				return true;
			}

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;

	}

	@Override
	public void addEmployee(String firstName, String lastName, String email, String username, String password) {
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "INSERT INTO employee(first_name,last_name,email,username,password,salt) VALUES (?,?,?,?,?,?)";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, firstName);
			ps.setString(2, lastName);
			ps.setString(3, email);
			ps.setString(4, username);
			try {
				byte[] s = passwordUtil.getSalt();
				ps.setString(5, passwordUtil.generateStrongPasswordHash(password, s));
				ps.setBytes(6, s);
			} catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			ps.execute();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public byte[] getSalt(String username) {
		byte[] s = null;
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT salt FROM employee WHERE username=?";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, username);
			ResultSet results = ps.executeQuery();
			if (results.next()) {
				s = results.getBytes(1);
			}
			return s;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

}
