package com.revature.dao;

import java.sql.Blob;

//import java.util.List;

//import com.revature.beans.Employee;

public interface EmployeeDao {
	
	public void addTicket(double amount,String tickDesc, byte[] tickReceipt,int tickHolder,int tickTypeId,int empId);
	public void approveTicket(int tickId,int employeeId);
	public void denyTicket(int tickId,int employeeId);
	public boolean checkEmployee(String username, String password);
	public void addEmployee(String firstName, String lastName,String email,String username, String password);
	public byte[] getSalt(String username);
	
	
}
