package com.revature.dao;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.revature.beans.Ticket;
import com.revature.util.ConnectionUtil;

public class TicketDaoImpl implements TicketDao {
	private ConnectionUtil connectionUtil = ConnectionUtil.getConnectionUtil();

	 private Ticket extractTicks(ResultSet results) throws SQLException {
		 Ticket tick = new Ticket();
		 tick.setTickAmount(results.getDouble("amount"));
		 tick.setTimeSub(results.getDate("time_sub"));
		 tick.setTickReceipt(results.getBytes("tick_receipt"));
		 tick.setTickDesc(results.getString("tick_desc"));
		 return tick;
		 }
	
	@Override
	public List<Ticket> viewTickets() {
		List<Ticket> ticks= new ArrayList<Ticket>();
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT t.amount,t.time_sub,t.tick_desc,t.tick_receipt FROM ticket t";
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet results=ps.executeQuery();
			while (results.next()) {
				 ticks.add(extractTicks(results));
			}
			return ticks;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public void sortPending() {
		// TODO Auto-generated method stub
		try (Connection conn = connectionUtil.getConnection()) {
			String sql = "SELECT t.amount,t.time_sub,t.tick_desc,t.tick_status_id\r\n" + 
					"FROM ticket t\r\n" + 
					"LEFT JOIN employee on  t.tick_holder= employee.emp_id\r\n" + 
					"ORDER BY tick_status_id=3 DESC\r\n";
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.execute();

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public List<Ticket> viewHistory(int empId) {
		List<Ticket> ticks= new ArrayList<Ticket>();
		try(Connection conn= connectionUtil.getConnection()){
			String sql= "SELECT t.amount,t.time_sub,t.tick_desc,t.tick_receipt FROM ticket t WHERE tick_holder=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, empId);
			ResultSet results=ps.executeQuery();
			while (results.next()) {
				 ticks.add(extractTicks(results));
			}
			return ticks;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	

	

	

}
