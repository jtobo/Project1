package com.revature.dao;

import java.util.List;

import com.revature.beans.Ticket;

public interface TicketDao {
	public List<Ticket> viewTickets();
	public void sortPending();
	public List<Ticket> viewHistory(int empId);
}
