package com.revature.beans;

import java.util.Arrays;
import java.sql.Date;

public class Ticket {
	private int tickId;
	private double tickAmount;
	private Date timeSub;
	private Date timeRes;
	private String tickDesc;
	private byte[] tickReceipt;
	private int tickHolder;
	private int tickSolver;
	private int tickStatusId;
	private int tickTypeId;
	
	
	public int getTickId() {
		return tickId;
	}
	public void setTickId(int tickId) {
		this.tickId = tickId;
	}
	public double getTickAmount() {
		return tickAmount;
	}
	public void setTickAmount(double tickAmount) {
		this.tickAmount = tickAmount;
	}
	public Date getTimeSub() {
		return timeSub;
	}
	public void setTimeSub(Date timeSub) {
		this.timeSub = timeSub;
	}
	public Date getTimeRes() {
		return timeRes;
	}
	
	public String getTickDesc() {
		return tickDesc;
	}
	public void setTickDesc(String tickDesc) {
		this.tickDesc = tickDesc;
	}
	public void setTimeRes(Date timeRes) {
		this.timeRes = timeRes;
	}
	public byte[] getTickReceipt() {
		return tickReceipt;
	}
	public void setTickReceipt(byte[] tickReceipt) {
		this.tickReceipt = tickReceipt;
	}
	public int getTickHolder() {
		return tickHolder;
	}
	public void setTickHolder(int tickHolder) {
		this.tickHolder = tickHolder;
	}
	public int getTickSolver() {
		return tickSolver;
	}
	public void setTickSolver(int tickSolver) {
		this.tickSolver = tickSolver;
	}
	public int getTickStatusId() {
		return tickStatusId;
	}
	public void setTickStatusId(int tickStatusId) {
		this.tickStatusId = tickStatusId;
	}
	public int getTickTypeId() {
		return tickTypeId;
	}
	public void setTickTypeId(int tickTypeId) {
		this.tickTypeId = tickTypeId;
	}
	public Ticket() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Ticket(int tickId, double tickAmount, Date timeSub, Date timeRes, String tickDesc, byte[] tickReceipt,
			int tickHolder, int tickSolver, int tickStatusId, int tickTypeId) {
		super();
		this.tickId = tickId;
		this.tickAmount = tickAmount;
		this.timeSub = timeSub;
		this.timeRes = timeRes;
		this.tickDesc = tickDesc;
		this.tickReceipt = tickReceipt;
		this.tickHolder = tickHolder;
		this.tickSolver = tickSolver;
		this.tickStatusId = tickStatusId;
		this.tickTypeId = tickTypeId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(tickAmount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + ((tickDesc == null) ? 0 : tickDesc.hashCode());
		result = prime * result + tickHolder;
		result = prime * result + tickId;
		result = prime * result + Arrays.hashCode(tickReceipt);
		result = prime * result + tickSolver;
		result = prime * result + tickStatusId;
		result = prime * result + tickTypeId;
		result = prime * result + ((timeRes == null) ? 0 : timeRes.hashCode());
		result = prime * result + ((timeSub == null) ? 0 : timeSub.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ticket other = (Ticket) obj;
		if (Double.doubleToLongBits(tickAmount) != Double.doubleToLongBits(other.tickAmount))
			return false;
		if (tickDesc == null) {
			if (other.tickDesc != null)
				return false;
		} else if (!tickDesc.equals(other.tickDesc))
			return false;
		if (tickHolder != other.tickHolder)
			return false;
		if (tickId != other.tickId)
			return false;
		if (!Arrays.equals(tickReceipt, other.tickReceipt))
			return false;
		if (tickSolver != other.tickSolver)
			return false;
		if (tickStatusId != other.tickStatusId)
			return false;
		if (tickTypeId != other.tickTypeId)
			return false;
		if (timeRes == null) {
			if (other.timeRes != null)
				return false;
		} else if (!timeRes.equals(other.timeRes))
			return false;
		if (timeSub == null) {
			if (other.timeSub != null)
				return false;
		} else if (!timeSub.equals(other.timeSub))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "Ticket [tickId=" + tickId + ", tickAmount=" + tickAmount + ", timeSub=" + timeSub + ", timeRes="
				+ timeRes + ", tickDesc=" + tickDesc + ", tickReceipt=" + Arrays.toString(tickReceipt) + ", tickHolder="
				+ tickHolder + ", tickSolver=" + tickSolver + ", tickStatusId=" + tickStatusId + ", tickTypeId="
				+ tickTypeId + "]";
	}
	
	
	
	
	
}
