package com.revature.util;

//import java.io.File;
//import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class ConnectionUtil {
	private static ConnectionUtil connectionUtil = new ConnectionUtil();
	private Properties properties;

	static {
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private ConnectionUtil() {

	}

	public static ConnectionUtil getConnectionUtil() {
		return connectionUtil;
	}

	public Connection getConnection() throws SQLException {
		InputStream is = ConnectionUtil.class.getClassLoader().getResourceAsStream("database.properties");
		properties = new Properties();
		try {
			properties.load(is);
		} catch (IOException e) {
			e.printStackTrace();
		}
		String url = System.getenv("URL_BANK");
		String user = System.getenv("USER_SQL");
		String password = System.getenv("PASSWORD_SQL");

		// get connection using properties data
		return DriverManager.getConnection(url, user, password);
	}

}
