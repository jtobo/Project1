package com.revature.services;

import java.sql.Blob;

import com.revature.dao.EmployeeDaoImpl;

public class EmployeeService {
	private static final EmployeeDaoImpl dao = new EmployeeDaoImpl();
	public void addTicket(double amount,String tickDesc, byte[] tickReceipt,int tickHolder,int tickTypeId,int empId){
		dao.addTicket(amount, tickDesc, tickReceipt, tickHolder, tickTypeId, empId);
	}
	public void approveTicket(int tickId,int employeeId) {
		dao.approveTicket(tickId, employeeId);
	}
	public void denyTicket(int tickId,int employeeId) {
		dao.denyTicket(tickId, employeeId);
	}
	public boolean checkEmployee(String username, String password) {
		return dao.checkEmployee(username, password);
	}
	public void addEmployee(String firstName, String lastName,String email,String username, String password) {
		dao.addEmployee(firstName, lastName, email, username, password);
	}
	public byte[] getSalt(String username) {
		return dao.getSalt(username);
	}
	
}
