package com.revature.services;

import java.util.List;

import com.revature.beans.Ticket;
import com.revature.dao.TicketDaoImpl;

public class TicketService {
	private static final TicketDaoImpl dao = new TicketDaoImpl();
	public List<Ticket> viewTickets(){
		
		return dao.viewTickets();
	}
	public void sortPending() {
		dao.sortPending();
	}
	public List<Ticket> viewHistory(int empId){
		return dao.viewHistory(empId);
	}
}
